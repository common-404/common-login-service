FROM openjdk:11.0.6-jdk
COPY build/libs/login-service-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "/app.jar"]
