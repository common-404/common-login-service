package com.common.loginservice.daos;

import com.common.loginservice.models.RoleModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleDAO extends MongoRepository<RoleModel, String> {

    Optional<RoleModel> findByRole(String role);
}
