package com.common.loginservice.daos;

import com.common.loginservice.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserDAO extends MongoRepository<UserModel, String> {

    Optional<UserModel> findByEmail(String email);
    Optional<UserModel> findByJwtToken(String jwtToken);
    List<UserModel> findByNameOrEmail(String name);

}
