package com.common.loginservice.controllers;

import com.common.loginservice.dtos.LoginDTO;
import com.common.loginservice.dtos.UserDTO;
import com.common.loginservice.services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/create")
    @PreAuthorize("hasAnyRole('ANONYMOUS', 'ADMIN')")
    public UserDTO createUser(@RequestBody UserDTO userDTO) {

        checkNotNull(userDTO);

        userService.createUser(userDTO);

        return userDTO;
    }

    @GetMapping("/info/{userId}")
    @PreAuthorize("hasAnyRole('ANONYMOUS', 'USER', 'ADMIN')")
    public UserDTO getUserInfo(@PathVariable String userId, HttpServletRequest request) {

        String tokenUnstripped = request.getHeader(AUTHORIZATION);
        String token = null;
        if(!isEmpty(tokenUnstripped)) {
            token = StringUtils.removeStart(tokenUnstripped, "Bearer").trim();
        }

        return userService.getUser(userId, token);
    }

    @PostMapping("/login")
    @PreAuthorize("hasAnyRole('ANONYMOUS')")
    public UserDTO loginUser(@RequestBody LoginDTO loginDTO) {

        checkNotNull(loginDTO);

        return userService.login(loginDTO);
    }

    @DeleteMapping("/delete/{userId}")
    @PreAuthorize("hasAnyRole('ANONYMOUS', 'ADMIN')")
    public void deleteUser(@PathVariable String userId, HttpServletRequest request) {
        userService.deleteUser(userId);
    }

    //- modify a user
    // TODO: homework

}
