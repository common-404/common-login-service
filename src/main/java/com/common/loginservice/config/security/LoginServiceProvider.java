package com.common.loginservice.config.security;

import com.common.loginservice.services.TokenService;
import com.common.loginservice.daos.UserDAO;
import com.common.loginservice.exceptions.InvalidTokenException;
import com.common.loginservice.exceptions.LoginServiceTokenAuthException;
import com.common.loginservice.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class LoginServiceProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    UserDAO userDAO;

    @Autowired
    TokenService tokenService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

    }

    @Override
    protected UserDetails retrieveUser(String email, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        final String token = (String) authentication.getCredentials();// this will be the jwtToken

        if (isEmpty(token)) {
            return new User(email, "", AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
        }

        // find user based on token
        Optional<UserModel> userModelOptional = userDAO.findByJwtToken(token);

        if (userModelOptional.isPresent()) {
            final UserModel userModel = userModelOptional.get();

            try {
                tokenService.validateToken(token);
            } catch (InvalidTokenException e) {
                userModel.setJwtToken(null);
                userDAO.save(userModel);

                return null;
            }

            return new User(email, "",
                    AuthorityUtils.createAuthorityList(
                            userModel.getRoles().stream()
                                    .map(roleName -> "ROLE_" + roleName)
                                    .toArray(String[]::new)
                    )
            );
        }

        throw new LoginServiceTokenAuthException("User not found for token :" + token);
    }

}
