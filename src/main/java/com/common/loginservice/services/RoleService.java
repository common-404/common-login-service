package com.common.loginservice.services;

import com.common.loginservice.dtos.RoleDTO;

public interface RoleService {

    void createRole(RoleDTO roleDTO);

    RoleDTO roleInfo(String roleId);

}
