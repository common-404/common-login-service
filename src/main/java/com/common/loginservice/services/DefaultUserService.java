package com.common.loginservice.services;

import com.common.loginservice.daos.RoleDAO;
import com.common.loginservice.daos.UserDAO;
import com.common.loginservice.dtos.LoginDTO;
import com.common.loginservice.dtos.UserDTO;
import com.common.loginservice.models.RoleModel;
import com.common.loginservice.models.UserModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;


@Service
public class DefaultUserService implements UserService {

    private static final String UNKNOWN_USERNAME_OR_BAD_PASSWORD = "Unknown username or bad password";

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    TokenService tokenService;

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void createUser(UserDTO userDTO) {

        checkNotNull(userDTO.getPassword());

        UserModel userModel = modelMapper.map(userDTO, UserModel.class);

        userModel.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));

        userModel.setRoles(
                roleDAO.findAll().stream()
                        .map(RoleModel::getRole)
                        .filter(role -> role.contains("USER"))
                        .collect(Collectors.toList())
        );

        tokenService.generateToken(userModel);

        userDAO.save(userModel);
        userDTO.setPassword("");
        modelMapper.map(userModel, userDTO);
    }

    @Override
    public UserDTO getUser(String userId, String token) {

        Optional<UserModel> optionalUserModel = userDAO.findById(userId);

        String userIdFromToken = this.tokenService.getUserIdFromToken(token);
        List<String> userRolesFromToken = this.tokenService.getRolesFromToken(token);

        if (optionalUserModel.isPresent()) {

            final UserModel userModel = optionalUserModel.get();

            if (userIdFromToken.equals(userId) || userRolesFromToken.contains("ADMIN")) {
                return modelMapper.map(userModel, UserDTO.class);
            }

            UserDTO userDTO = new UserDTO();
            userDTO.setName(userModel.getName());
            return userDTO;
        }

        return null;
    }

    @Override
    public void deleteUser(String userId) {
        Optional<UserModel> optionalUserModel = userDAO.findById(userId);

        if (optionalUserModel.isPresent()) {
            userDAO.delete(optionalUserModel.get());
        }

    }

    @Override
    public UserDTO login(LoginDTO loginDTO) {

        Optional<UserModel> optionalUserModel = userDAO.findByEmail(loginDTO.getEmail());

        if (optionalUserModel.isPresent()) {
            UserModel userModel = optionalUserModel.get();

            if (bCryptPasswordEncoder.matches(loginDTO.getPassword(), userModel.getPassword())) {

                return modelMapper.map(userModel, UserDTO.class);
            } else {
                throw new BadCredentialsException(UNKNOWN_USERNAME_OR_BAD_PASSWORD);
            }
        } else {
            throw new BadCredentialsException(UNKNOWN_USERNAME_OR_BAD_PASSWORD);
        }
    }

}
