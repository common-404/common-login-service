package com.common.loginservice.services;

import com.common.loginservice.exceptions.InvalidTokenException;
import com.common.loginservice.models.UserModel;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.SignatureException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Service
@Log4j2
public class DefaultTokenService implements TokenService {

    @Autowired
    LoginServiceSigningKeyResolver loginServiceSigningKeyResolver;

    @Override
    public void validateToken(String jwtToken) throws InvalidTokenException {

        checkNotNull(jwtToken);

        try {
            Jwts.parserBuilder()
                    .setSigningKeyResolver(loginServiceSigningKeyResolver)
                    .build()
                    .parse(jwtToken);

        } catch (ExpiredJwtException | MalformedJwtException | SignatureException | IllegalArgumentException e) {
            log.warn("Exception at token validation");
            throw new InvalidTokenException("Invalid token", e);
        }
    }

    @Override
    public void generateToken(UserModel userModel) {

        String jwtToken;
        jwtToken = Jwts.builder()
                .setSubject(userModel.getEmail())
                .setAudience(userModel.getRoles().toString())
                .setIssuer(userModel.getId())
                .signWith(loginServiceSigningKeyResolver.getSecretKey(), SignatureAlgorithm.HS512)
                .compact();

        userModel.setJwtToken(jwtToken);
    }

    @Override
    public List<String> getRolesFromToken(String jwtToken) {

        if (isEmpty(jwtToken)) {
            return new ArrayList<>();
        }

        String claims = new String(Base64.getUrlDecoder().decode(jwtToken.split("\\.")[1]));
        JSONObject claimsJson = new JSONObject(claims);

        // "[ADMIN, USER]"
        String audience = claimsJson.getString("aud");
        final String[] split = audience
                .replace("[", "")
                .replace("]", "")
                .split(",");

        return Stream.of(split).map(String::trim).collect(Collectors.toList());
    }

    @Override
    public String getUserIdFromToken(String jwtToken) {
        if (isEmpty(jwtToken)) {
            return StringUtils.EMPTY;
        }

        // abc.123.awe
        String claims = new String(Base64.getUrlDecoder().decode(jwtToken.split("\\.")[1]));
        JSONObject claimsJson = new JSONObject(claims);

        return claimsJson.getString("iss");
    }

}
