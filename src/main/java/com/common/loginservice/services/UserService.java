package com.common.loginservice.services;

import com.common.loginservice.dtos.LoginDTO;
import com.common.loginservice.dtos.UserDTO;

public interface UserService {

    void createUser(UserDTO userDTO);

    UserDTO getUser(String userId, String token);

    void deleteUser(String userId);

    UserDTO login(LoginDTO loginDTO);
}
