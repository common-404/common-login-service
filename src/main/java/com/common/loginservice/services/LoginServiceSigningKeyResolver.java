package com.common.loginservice.services;

import io.jsonwebtoken.SigningKeyResolver;

import javax.crypto.SecretKey;

public interface LoginServiceSigningKeyResolver extends SigningKeyResolver {

    SecretKey getSecretKey();

}

