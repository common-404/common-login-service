package com.common.loginservice.services;

import com.common.loginservice.dtos.RoleDTO;
import com.common.loginservice.daos.RoleDAO;
import com.common.loginservice.models.RoleModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DefaultRoleService implements RoleService {

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public void createRole(RoleDTO roleDTO) {

        RoleModel roleModel = modelMapper.map(roleDTO, RoleModel.class);

        roleDAO.save(roleModel);

        modelMapper.map(roleModel, roleDTO);
    }

    @Override
    public RoleDTO roleInfo(String roleId) {

        Optional<RoleModel> roleModelOptional = roleDAO.findById(roleId);

        if (roleModelOptional.isPresent()) {
            final RoleModel roleModel = roleModelOptional.get();

            return modelMapper.map(roleModel, RoleDTO.class);
        }

        return null;
    }
}


