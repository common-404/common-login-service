package com.common.loginservice.services;

import com.common.loginservice.exceptions.InvalidTokenException;
import com.common.loginservice.models.UserModel;

import java.util.List;

public interface TokenService {

    void validateToken(String jwtToken) throws InvalidTokenException;

    void generateToken(UserModel userModel);

    List<String> getRolesFromToken(String jwtToken);

    String getUserIdFromToken(String jwtToken);

}
