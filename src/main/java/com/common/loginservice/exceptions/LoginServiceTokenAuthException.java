package com.common.loginservice.exceptions;

import org.springframework.security.core.AuthenticationException;

public class LoginServiceTokenAuthException extends AuthenticationException {
    public LoginServiceTokenAuthException(String s) {
        super(s);
    }
}
