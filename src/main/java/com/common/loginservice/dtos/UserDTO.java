package com.common.loginservice.dtos;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserDTO {

    private String name;
    private String password;
    private List<String> roles;
    private String email;
    private Date createdDate;
    private Date lastModifiedDate;
    private String id;
    private String jwtToken;
}
