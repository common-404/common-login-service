package com.common.loginservice.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class RoleDTO {

    private String role;
    private String id;
    private Date createdDate;
    private Date lastModifiedDate;

}
