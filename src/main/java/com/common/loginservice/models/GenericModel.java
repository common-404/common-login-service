package com.common.loginservice.models;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.domain.Persistable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
public class GenericModel implements Serializable, Persistable<String> {

    @Id
    private String id;

    @CreatedDate
    private Date createdDate;

    @LastModifiedDate
    private Date lastModifiedDate;

    @Override
    public boolean isNew() {
        return false;
    }

    public GenericModel() {
        this.id = UUID.randomUUID().toString();
    }
}
