package com.common.loginservice.initialise;

import com.common.loginservice.models.RoleModel;
import com.common.loginservice.services.TokenService;
import com.common.loginservice.daos.RoleDAO;
import com.common.loginservice.daos.UserDAO;
import com.common.loginservice.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Profile({"dev", "test"})
@Component
public class InitialiseTestData {

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    TokenService tokenService;


    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        addRoles();
        addUsers();
    }

    void addRoles() {

        roleDAO.deleteAll();

        RoleModel role1 = new RoleModel();
        role1.setRole("ADMIN");

        RoleModel role2 = new RoleModel();
        role2.setRole("USER");

        roleDAO.save(role2);
        roleDAO.save(role2);
    }

    void addUsers() {

        userDAO.deleteAll();

        UserModel user1 = new UserModel();
        user1.setEmail("admin@common-login-service.com");
        user1.setPassword(bCryptPasswordEncoder.encode("admin"));
        user1.setRoles(Collections.singletonList("ADMIN"));
        user1.setId("c3191105-1803-437c-9720-e94d37efa429");
        tokenService.generateToken(user1);
        user1.setName("adminName");

        userDAO.save(user1);

        UserModel user2 = new UserModel();
        user2.setEmail("user@common-login-service.com");
        user2.setPassword(bCryptPasswordEncoder.encode("user"));
        user2.setRoles(Collections.singletonList("USER"));
        user2.setId("1lk23j1kl3j-lkkj3lk124j3kl2-kj234lk23j4");
        tokenService.generateToken(user2);
        user2.setName("userName");

        userDAO.save(user2);
    }


}
