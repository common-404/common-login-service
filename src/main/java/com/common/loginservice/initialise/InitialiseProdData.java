package com.common.loginservice.initialise;

import com.common.loginservice.models.RoleModel;
import com.common.loginservice.services.TokenService;
import com.common.loginservice.daos.RoleDAO;
import com.common.loginservice.daos.UserDAO;
import com.common.loginservice.models.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Optional;

@Profile({"prod"})
@Component
public class InitialiseProdData {

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleDAO roleDAO;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    TokenService tokenService;


    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        addRoles();
        addUsers();
    }

    void addRoles() {

        final Optional<RoleModel> optionalRoleAdmin = roleDAO.findByRole("ADMIN");
        if (!optionalRoleAdmin.isPresent()) {
            RoleModel roleAdmin = new RoleModel();
            roleAdmin.setRole("ADMIN");
            roleDAO.save(roleAdmin);
        }

        final Optional<RoleModel> optionalRoleUser = roleDAO.findByRole("USER");
        if (!optionalRoleUser.isPresent()) {
            RoleModel roleUser = new RoleModel();
            roleUser.setRole("USER");
            roleDAO.save(roleUser);
        }
    }

    void addUsers() {

        final Optional<UserModel> optionalUserAdmin = userDAO.findByEmail("admin@common-login-service.com");

        if (!optionalUserAdmin.isPresent()) {
            UserModel userAdmin = new UserModel();
            userAdmin.setEmail("admin@common-login-service.com");
            userAdmin.setPassword(bCryptPasswordEncoder.encode("12j3hka838ah"));
            userAdmin.setRoles(Collections.singletonList("ADMIN"));
            tokenService.generateToken(userAdmin);
            userAdmin.setName("adminNName");

            userDAO.save(userAdmin);
        }

        final Optional<UserModel> optionalUserUser = userDAO.findByEmail("user@common-login-service.com");

        if (!optionalUserUser.isPresent()) {
            UserModel userUser = new UserModel();
            userUser.setEmail("user@common-login-service.com");
            userUser.setPassword(bCryptPasswordEncoder.encode("2kj345kjhsdfuia"));
            userUser.setRoles(Collections.singletonList("USER"));
            tokenService.generateToken(userUser);
            userUser.setName("userName");

            userDAO.save(userUser);
        }
    }

}
